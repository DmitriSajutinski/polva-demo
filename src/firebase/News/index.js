import { db } from "../index.js";
import { collection, getDocs, onSnapshot} from 'firebase/firestore'

const newsObjectList = []

/*const querySnapshot = await getDocs(collection(db, 'news'));
querySnapshot.forEach((doc) => {
    // doc.data() is never undefined for query doc snapshots
    const newsObject = {
        id: doc.id,
        title: doc.data().title,
        content: doc.data().content
    }
    newsObjectList.push(newsObject)
    console.log(doc.id, " => ", doc.data());
});*/

onSnapshot(collection(db, 'news'), (querySnapShot) => {
    querySnapShot.forEach((doc) => {
        const newsObject = {
            id: doc.id,
            title: doc.data().title,
            content: doc.data().content
        }
        newsObjectList.push(newsObject)
    })
    console.log(newsObjectList, '============')
    return newsObjectList
})

export {
    newsObjectList,
}