// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBlsI39Jzws2Pa3H6ldVEDbaVgMudyKjNY",
    authDomain: "polva-haigla-demo.firebaseapp.com",
    projectId: "polva-haigla-demo",
    storageBucket: "polva-haigla-demo.appspot.com",
    messagingSenderId: "377891435402",
    appId: "1:377891435402:web:eccb8fbe65b28209a44e73",
    measurementId: "G-JHTPKCKV32"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

// Initialize Firestore
const db = getFirestore(app)

export {
    db
}